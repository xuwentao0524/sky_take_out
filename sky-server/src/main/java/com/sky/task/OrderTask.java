package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@Component
public class OrderTask {
    @Autowired
    OrderMapper orderMapper;

    //超时未支付订单处理
    //出发时间每分钟
    @Scheduled(cron = "0/5 * * * * ?")
    public void OutTimeNotPay() {
        //订单状态 1待付款 2待接单 3已接单 4派送中 5已完成 6已取消
        //获取当前时间,计算出超时时间节点
        LocalDateTime time = LocalDateTime.now().minusMinutes(15);
        //统计派送中订单
        List<Orders> orders = orderMapper.selectByTimeAndStatus(Orders.PENDING_PAYMENT, time);
        //2.修改订单状态
        if (orders != null && orders.size() > 0) {
            orders.forEach(o -> {
                System.out.println("正在取消超时订单:" + o.getNumber());
                orderMapper.update(
                        Orders.builder()
                                .id(o.getId())  //设置需要更新订单的id
                                .status(Orders.CANCELLED)
                                .cancelReason("超时取消")
                                .cancelTime(LocalDateTime.now())
                                .build());

            });
        }
    }

    //超时未完成订单处理
    //每天三点
    @Scheduled(cron = "* * 3 * * ? ")
    public void OutTimeNotConfirm() {
        //订单状态 1待付款 2待接单 3已接单 4派送中 5已完成 6已取消
        //获取当前时间

        LocalDateTime time = LocalDateTime.now().minusHours(3);//三点触发方法,往前推三小时
        //统计超时订单
        List<Orders> orders = orderMapper.selectByTimeAndStatus(Orders.DELIVERY_IN_PROGRESS, time);
        //2.修改订单状态
        if (orders != null && orders.size() > 0) {
            orders.forEach(o -> {
                System.out.println("正在完成派送订单:" + o.getNumber());
                orderMapper.update(
                        Orders.builder()
                                .id(o.getId())  //设置需要更新订单的id
                                .status(Orders.COMPLETED)
                                .build());

            });
        }
    }
}