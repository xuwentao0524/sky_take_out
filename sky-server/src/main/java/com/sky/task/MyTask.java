package com.sky.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MyTask {
//    @Scheduled(cron = "0/5 * * * * ? ")
    public void printTime(){
        System.out.println("当前系统时间:"+ LocalDateTime.now());
    }
}
