package com.sky.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sky.constant.WxConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.LoginFailedException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    WeChatProperties weChatProperties;
    @Autowired
    UserMapper userMapper;
    @Override
    public User checkLogin(UserLoginDTO userLoginDTO) {
        //1.使用httpclient,和为服务器交互,验证code是否正确
        Map<String,String> param = new HashMap<>();
        param.put("appid",weChatProperties.getAppid());//小程序appID
        param.put("secret",weChatProperties.getSecret());//小程序appSecret
        param.put("js_code", userLoginDTO.getCode());//登陆时获取的code
        param.put("grant_type","authorization_code");//授权类型,固定格式

        String wxResult = HttpClientUtil.doGet(WxConstant.CODE_CHECK_URL,param);
        log.info("微信服务验证code的结果:{}",wxResult);//{"session_key":"JK6oAb4PoB6OBHJxmdzUUg==","openid":"oAoHN4hiBWHyEjOZBg-4HUMOunGo"}
        //2.判断结果openid是否存在
        //如果不存在,微信用户不合法,直接抛异常
        String openid = JSONObject.parseObject(wxResult).getString("openid");//获取json中opneid
        if(openid == null){//如果openid为空
            throw new LoginFailedException("非法微信用户");
        }

        //3.判断当前微信用户是否为新用户
        //如果是新用户,添加用户信息到user表中
        User user = userMapper.selectByOpenId(openid);

        //判断如果为新用户添加数据库中再返回,如果不为新用户直接返回
        if (user == null){
            //查询user数据库,如果user为空则表示为新用户
            user=User.builder()
                    .openid(openid)
                    .createTime(LocalDateTime.now())
                    .build();
            userMapper.insert(user);
        }
        //4.封装返回验证结果
        return user;
    }
}
