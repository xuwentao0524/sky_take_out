package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Autowired
    ShoppingCartMapper shoppingCartMapper;
    @Autowired
    DishMapper dishMapper;
    @Autowired
    SetmealMapper setmealMapper;

    @Override
    public void add(ShoppingCartDTO shoppingCartDTO) {

        //1.查询当前添加得商品再当前用户的购物车列表中是否存在
        //select * from shopping_cart where user_id=? dish_id=? and dish_flover=?
        //select * from shopping_cart where user_id=? and setmeal_id=?

        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO, shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        //目前只能查询到一个商品信息,为了方法得复用性,设置成了一个集合
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByCondition(shoppingCart);
        if (shoppingCartList != null && shoppingCartList.size() > 0) {
            //2.判断当前商品是否存在,如果存在数量+1
            ShoppingCart updateShoppingCart = ShoppingCart.builder()
                    .number(shoppingCartList.get(0).getNumber() + 1)
                    .id(shoppingCartList.get(0).getId())
                    .build();
            shoppingCartMapper.update(updateShoppingCart);
        } else {


            //3.如果不存在,添加当前商品到购物车

            ShoppingCart addShoppingCart = new ShoppingCart();
            addShoppingCart.setNumber(1);
            addShoppingCart.setCreateTime(LocalDateTime.now());
            addShoppingCart.setUserId(BaseContext.getCurrentId());

            //判断查询出得dishid如果不为null,则表示为菜品
            if (shoppingCartDTO.getDishId() != null) {
                Dish dish = dishMapper.getById(shoppingCart.getDishId());

                addShoppingCart.setDishFlavor(shoppingCartDTO.getDishFlavor());
                addShoppingCart.setDishId(dish.getId());
                addShoppingCart.setImage(dish.getImage());
                addShoppingCart.setAmount(dish.getPrice());
                addShoppingCart.setName(dish.getName());
            } else {
                //否则为套餐
                Setmeal setmeal = setmealMapper.getById(shoppingCart.getSetmealId());
                addShoppingCart.setSetmealId(setmeal.getId());
                addShoppingCart.setImage(setmeal.getImage());
                addShoppingCart.setAmount(setmeal.getPrice());
                addShoppingCart.setName(setmeal.getName());
            }
            shoppingCartMapper.insert(addShoppingCart);
        }
    }

    @Override
    public List<ShoppingCart> showShoppingCart() {

        return shoppingCartMapper.selectByCondition(ShoppingCart.builder()
                .userId(BaseContext.getCurrentId())
                .build());
    }

    @Override
    public void cleanShoppingCart() {
        shoppingCartMapper.cleanShoppingCart(BaseContext.getCurrentId());

    }

    @Override
    public void subShoppingCart(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCart,shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        //目前只能查询到一个商品信息,为了方法得复用性,设置成了一个集合
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.selectByCondition(shoppingCart);

        if (shoppingCartList != null && shoppingCartList.size()>0){
            shoppingCart = shoppingCartList.get(0);
            Integer number = shoppingCart.getNumber();
            if(number == 1){
                shoppingCartMapper.deleteById(shoppingCart.getId());
            }else {
                shoppingCart.setNumber(shoppingCart.getNumber()-1);
                shoppingCartMapper.updateNumberById(shoppingCart);
            }
        }
    }
}
