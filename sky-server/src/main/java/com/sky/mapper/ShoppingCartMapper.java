package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.io.Serializable;
import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    List<ShoppingCart> selectByCondition(ShoppingCart shoppingCart);

    @Update("update shopping_cart set number = #{number} where id = #{id}")
    void update(Serializable updateShoppingCart);

    @Insert("insert into shopping_cart (id,name, user_id, dish_id, setmeal_id, dish_flavor, number, amount, image, create_time) " +
            " values (null,#{name},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{number},#{amount},#{image},#{createTime})")
    void insert(ShoppingCart addShoppingCart);

    @Delete("delete from shopping_cart where user_id = #{userId}")
    void cleanShoppingCart(Long currentId);

    @Delete("delete from shopping_cart where id = #{id}")
    void deleteById(Long id);

    @Update("update shopping_cart set number = #{number} where id = #{id}")
    void updateNumberById(ShoppingCart shoppingCart);

    void inserts(List<ShoppingCart> shoppingCarts);
}
