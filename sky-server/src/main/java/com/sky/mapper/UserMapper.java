package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
    /**
     * 根据openid查询
     * @param openid
     * @return
     */
    @Select("select * from user where openid = #{openid}")
    User selectByOpenId(String openid);

    /**
     * 添加用户信息
     * @param user
     */
    void insert(User user);
}
